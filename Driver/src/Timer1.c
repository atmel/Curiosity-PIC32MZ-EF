#include <xc.h>
#include "../inc/Timer1.h"
/*
 * Timer1��ʼ������ *************************************************************
 */
void Timer1_Init(void) {
    /* Set Timer 1 period register */
    PR1 = T1_TICK_PERIOD - 1;
    /* Timer1: Internal clock, 1:256 pre-scaler, Timer mode, Enable Timer1 */
    T1CON = 0x8030;
}
