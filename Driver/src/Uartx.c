#include "xc.h"
#include <stdio.h>              // 使用printf函数
#include "../../Driver/inc/Uartx.h"

/*
 * 变量声明 *********************************************************************
 */
// UART2默认任务
UART2_TASK_STATUS UART2_TASK_ID = UART2_IDLE;

// UART2数据FIFO定义
unsigned char UART2_BUFFER[128]; // UART2接收FIFO缓冲区
unsigned int ptr_UART2_head; // FIFO头指针
unsigned int ptr_UART2_tail; // FIFO尾指针

/*
 * Uart2 初始化
 * 参数：baudrate -- 设置波特率
 * 返回值：由于波特率存在小数，实际波特率存在误差，返回值是实际得到的波特率。
 */
unsigned int UART2_Init(unsigned int baudrate)
{
    unsigned int actual_baud = 0;
    // UxBRG = PBCLK2 / ( 16 * baud ) ) - 1
    U2BRG = PBCLK2 / (16 * baudrate) + 0.5 - 1;
    //计算实际产生的波特率
    actual_baud = PBCLK2 / (16 * (U2BRG + 1));
	U2MODE = 0x8000; 	// 使能UART2：8-bit data, no parity, 1 stop bit
    U2STA = 0x1400; 	// 使能UART2收发功能
    
    return actual_baud;
}

/*
 * Uart2 任务（状态机模式）
 * 
 */
void UART2_Task(void) {
    // 任务转换
    switch (UART2_TASK_ID) {
        case UART2_IDLE:
            if (ptr_UART2_head != ptr_UART2_tail)
                UART2_TASK_ID = UART2_BUFFER_NOT_EMPTY; //转换为缓冲区不为空
            break;
        case UART2_BUFFER_NOT_EMPTY:
            // 发送缓冲区中的所有数据
            while (ptr_UART2_head != ptr_UART2_tail) {
                // 送回发送端
                putchar(UART2_BUFFER[ptr_UART2_tail++]);
                // 维护FIFO指针
                UART2_RX_FIFO_Ptr_Update();
            }
            UART2_TASK_ID = UART2_IDLE; //转换为空闲模式
            break;
    }
}

/*
 * FIFO初始化（主要是初始化两个指针变量为0）
 */
void UART2_RX_FIFO_Init(void)
{
    ptr_UART2_head = 0;
    ptr_UART2_tail = 0;
}

/*
 * 维护FIFO头尾指针
 * 只要执行了数据进出队列，就需要执行一次FIFO指针维护
 */
void UART2_RX_FIFO_Ptr_Update(void)
{
    if (ptr_UART2_head == sizeof (UART2_BUFFER))
        ptr_UART2_head = 0;    

    if (ptr_UART2_tail == sizeof (UART2_BUFFER))
        ptr_UART2_tail = 0;                    
}

/* *****************************************************************************
 End of File
 */
