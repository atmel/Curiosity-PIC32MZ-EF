/* 
 * File:   main.c
 * Author: jiushu
 * Email: jiushu@tsinghua.info
 * 
 * Brief：
 * 本项目使用Curiosity PIC32 MZ EF开发板，实现如下功能：
 * （1）Timer1定时器实现 10 ms 嘀嗒定时（中断实现）
 * （2）LED1在50个滴答周期翻转一次状态，即实现LED1以1 Hz频率闪烁
 * （3）UART2通过串口接收中断接收数据，然后原封不同通过UART2发送
 * （4）S1按键控制LED4（三色LED）的颜色切换
 * （5）程序使用状态机切换，无阻塞
 * 
 * Created on 2017年10月20日, 上午11:37
 */

/* 
 * 包含头文件 *******************************************************************
 */
#include "../user/inc/ConfigureBits.h"          // 配置位头文件
#include <xc.h>                 // XC32器件定义头文件
#include <stdio.h>              //使用putchar、printf等C语言函数
// 外设驱动头文件
#include "../Driver/inc/Bsp.h"          // 板级接口定义
#include "../Driver/inc/Timer1.h"       // Timer1接口
#include "../Driver/inc/Uartx.h"        // Uart接口

/*
 * 函数声明 *********************************************************************
 */
void PPS_Init(void);        // 外设引脚选择（引脚重映射）

/*
 * 入口函数 *********************************************************************
 */
void main(void) {
    // 1. 外设初始化 ////////////////////////////////////////////////////////////
    UART2_RX_FIFO_Init(); // UART2 FIFO初始化
    UART2_Init(115200); // UART2 初始化
    Timer1_Init(); // Timer1 初始化
    LED_Init(); // LED 初始化
    S1_Init(); // S1初始化
    PPS_Init(); // 引脚重映射

    // 2. 中断配置 //////////////////////////////////////////////////////////////
    INTCONbits.MVEC = 1;    // 配置中断为多向量模式（必须放在使能中断之前）
    Interrupt_Configure(); // 外设中断配置
    __builtin_enable_interrupts(); // 使能全局中断（等于asm("ei")）

    // 3. 测试信息
    printf("Welcome to the PIC32 world!\r\n");

    // 4. 超级循环
    while (1) {
        UART2_Task(); // UART2任务
        LED1_TASK(); // LED1任务
        S1_Task(); // S1任务
    } // 超级循环结束
}// main函数结束

/*
 * 引脚重映射********************************************************************
 */
void PPS_Init(void) {
    // 1. UART2 引脚重映射///////////////////////////////////////////////////////
    // UART2_TX <--> RPC2
    // UART2_RX <--> RPC3
    // 1.1. 配置RPn引脚为输入引脚时，TRISx寄存器中的对应位必须配置为输入
    TRISCbits.TRISC3 = 1;
    // 1.2. 可重映射外设的优先级永远不会高于与该引脚相关的任何模拟功能
    //   所以我们需要将该引脚配置为数字功能
    ANSELCbits.ANSC3 = 0;
    // 1.3. 引脚重映射
    U2RXRbits.U2RXR = 0b01100; // UART2 Rx引脚映射到RPC3引脚
    RPC2Rbits.RPC2R = 2; // UART2 Tx引脚映射到RPC2引脚   
}

/* *****************************************************************************
 End of File
 */
