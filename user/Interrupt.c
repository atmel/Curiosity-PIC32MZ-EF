#include "xc.h"
#include <sys/attribs.h>    //使用__ISR()宏定义需要包含该头文件
#include "../Driver/inc/Bsp.h"
#include "../Driver/inc/Uartx.h"

/*
 * 变量定义**********************************************************************
 */
// UART2数据FIFO定义
extern unsigned char UART2_BUFFER[128]; // UART2接收FIFO缓冲区
extern unsigned int ptr_UART2_head; // FIFO头指针
extern unsigned int ptr_UART2_tail; // FIFO尾指针

// UART2任务状态
extern UART2_TASK_STATUS UART2_TASK_ID; // UART2默认空闲状态
extern S1_TASK_STATUS S1_TASK_ID; // S1默认为未按下
extern LED1_TASK_STATUS LED1_TASK_ID; // LED1当前状态不改变

/*
 * 中断配置**********************************************************************
 */
void Interrupt_Configure(void) {
    // Timer 1 中断配置: 中选优先级1，子优先级0 //////////////////////////////////
    IPC1bits.T1IP = 1;
    IPC1bits.T1IS = 0;
    IFS0bits.T1IF = 0;
    IEC0bits.T1IE = 1;

    // uart2 中断配置: 中断优先级2，子优先级0 ////////////////////////////////////
    IPC36bits.U2RXIP = 2;
    IPC36bits.U2RXIS = 0;
    IFS4bits.U2RXIF = 0;
    IEC4bits.U2RXIE = 1;
}

/*
 *  Timer 1 Interrupt handler
 *  优先级1--最低优先级
 *  实现SysTick滴答功能，滴答周期10ms
 *  在滴答周期中做如下事情：
 * （1）滴答变量累加
 * （2）定时改变LED1状态
 * （3）每个滴答周期检测按键S1的状态
 */
void __ISR(_TIMER_1_VECTOR, IPL1AUTO) Timer1Handler(void) {
    static unsigned int timer_tick = 0; // 滴答定时器变量
    static char S1_status = 1; //S1默认高电平（上拉电阻，见原理图）

    timer_tick++;       // 滴答累积
    
    //计时到了时间改变LED1的状态
    if (50 == timer_tick) {
        timer_tick = 0;
        LED1_TASK_ID = LED1_INV; // LED1任务状态改变
    }

    // 判断S1是否按下(检测下降沿))
    if ((S1_STA == 0) && (S1_status == 1)) {
        S1_TASK_ID = S1_PRESS;  // 按键S1任务状态改变
    }
    S1_status = S1_STA; // 实时获取S1状态

    IFS0bits.T1IF = 0;  // 手工清除Timer1中断标志位
} // Timer 1 Interrupt handler结束

/*
 *  Uart 2 Interrupt handler
 *  优先级2
 *  UART2 收到数据后，推进FIFO缓冲区，提高对串口数据的处理效率
 */
void __ISR(_UART2_RX_VECTOR, IPL2AUTO) Uart2Handler(void) {
    // 接收到的数据存储到FIFO缓冲区
    UART2_BUFFER[ptr_UART2_head++] = U2RXREG;
    UART2_RX_FIFO_Ptr_Update(); // 更新FIFO指针
    LED2_INV(); //反转LED2的状态，指示UART2接收数据状态

    IFS4bits.U2RXIF = 0; // 清除中断标志位
} // Uart2 RX Interrupt handler结束

/* *****************************************************************************
 End of File
 */
