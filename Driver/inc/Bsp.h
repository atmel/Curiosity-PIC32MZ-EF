#ifndef _BSP_H    /* Guard against multiple inclusion */
#define _BSP_H

/*
 * 宏定义 ***********************************************************************
 */
// LED接口定义
#define LED1_ENABLE()  TRISEbits.TRISE3 = 0
#define LED1_ON()   LATEbits.LATE3 = 1
#define LED1_OFF()   LATEbits.LATE3 = 0
#define LED1_INV()   LATEbits.LATE3 ^= 1
#define LED2_ENABLE()  TRISEbits.TRISE4 = 0
#define LED2_ON()   LATEbits.LATE4 = 1
#define LED2_OFF()   LATEbits.LATE4 = 0
#define LED2_INV()   LATEbits.LATE4 ^= 1
#define LED3_ENABLE()  TRISEbits.TRISE6 = 0
#define LED3_ON()   LATEbits.LATE6 = 1
#define LED3_OFF()   LATEbits.LATE6 = 0
#define LED3_INV()   LATEbits.LATE6 ^= 1

#define LED4_BLUE_ENABLE()  TRISBbits.TRISB0 = 0
#define LED4_BLUE_ON()    LATBbits.LATB0 = 0
#define LED4_BLUE_OFF()   LATBbits.LATB0 = 1
#define LED4_BLUE_INV()   LATBbits.LATB0 ^= 1
#define LED4_GREEN_ENABLE()  TRISBbits.TRISB1 = 0
#define LED4_GREEN_ON()    LATBbits.LATB1 = 0
#define LED4_GREEN_OFF()   LATBbits.LATB1 = 1
#define LED4_GREEN_INV()   LATBbits.LATB1 ^= 1
#define LED4_RED_ENABLE()  TRISBbits.TRISB5 = 0
#define LED4_RED_ON()    LATBbits.LATB5 = 0
#define LED4_RED_OFF()   LATBbits.LATB5 = 1
#define LED4_RED_INV()   LATBbits.LATB5 ^= 1
#define LED4_OFF()      LED4_RED_OFF();LED4_GREEN_OFF();LED4_BLUE_OFF();

// S1按键接口
#define S1_ENABLE()         TRISGbits.TRISG12 = 1;
#define S1_STA              PORTGbits.RG12

// LED1任务状态
typedef enum {
    LED1_FREE = 0,
    LED1_INV
} LED1_TASK_STATUS;

// S1任务状态
typedef enum {
    S1_NO_PRESS = 0,
    S1_PRESS
} S1_TASK_STATUS;

/*
 * 接口函数 *********************************************************************
 */
void LED_Init(void);
void LED1_TASK(void);
void S1_Init(void);
void S1_Task(void);

#endif /* _BSP_H */

/* *****************************************************************************
 End of File
 */
