#ifndef _DRIVER_TIMER_H    /* Guard against multiple inclusion */
#define _DRIVER_TIMER_H

// Timer 1 ���� ////////////////////////////////////////////////////////////////
#define PBCLK3                  100000000
#define T1_CLK                  PBCLK3
#define T1_TICK_FREQUENCY       100
#define T1_TICK_PERIOD         (T1_CLK / ( 256 * T1_TICK_FREQUENCY))

// �����ӿ� /////////////////////////////////////////////////////////////////////
void Timer1_Init(void);

#endif /* _DRIVER_TIMER_H */

/* *****************************************************************************
 End of File
 */
