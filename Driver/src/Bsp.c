#include <xc.h>
#include <stdio.h>
#include "../../Driver/inc/Bsp.h"

/*
 * 变量初始化 *******************************************************************
 */
LED1_TASK_STATUS LED1_TASK_ID = LED1_FREE; 
S1_TASK_STATUS S1_TASK_ID = S1_NO_PRESS; // S1默认为未按下

/*
 * LED 初始化 ******************************************************************
 */
void LED_Init(void) {
    LED1_ENABLE();
    LED2_ENABLE();
    LED3_ENABLE();
    LED4_BLUE_ENABLE();
    LED4_GREEN_ENABLE();
    LED4_RED_ENABLE();

    LED1_OFF();
    LED2_OFF();
    LED3_OFF();
    LED4_OFF();
}

/*
 * LED1 任务
 * 当任务状态变为LED1_INV时反转LED1，再把LED1任务转换到LED1_FREE
 */
void LED1_TASK(void) {
    if (LED1_TASK_ID == LED1_INV) {
        LED1_INV();
        LED1_TASK_ID = LED1_FREE;
    }
}

/*
 * LED4 发光
 * color -- 控制LED4的发光颜色，0--熄灭；1--红色；2--绿色；3--蓝色
 */
unsigned char LED4_Glow(unsigned char color) {
    switch (color) {
        case 0: LED4_OFF();
            printf("Off.\r\n");
            break;
        case 1: LED4_OFF();LED4_RED_ON();
            printf("Red.\r\n");
            break;
        case 2: LED4_OFF();LED4_GREEN_ON();
            printf("Green.\r\n");
            break;
        case 3: LED4_OFF();LED4_BLUE_ON();
            printf("Blue.\r\n");
            break;
        default: LED4_OFF();
            printf("ERR!\r\n");
    }
    return color;
}

/*
 * S1 初始化
 * 将S1接口设置为输入状态，S1接口默认接上拉电阻（见原理图）
 */
void S1_Init(void) {
    S1_ENABLE();
}

/*
 * S1任务
 * 当S1切换到S1_PRESS时，点亮LED4，再将任务切换到S1_NO_PRESS；点亮LED4是有规则的：
 * （1）启动后LED4默认熄灭
 * （2）每按下一次S1，LED4颜色改变一次，改变规律：熄灭->红->绿->蓝，循环
 */
void S1_Task(void) {
    static unsigned char LED4_Color = 1;
    if (S1_TASK_ID == S1_PRESS) {
        LED4_Color = LED4_Glow(LED4_Color) + 1;
        if (LED4_Color >= 4)
            LED4_Color = 0;
        S1_TASK_ID = S1_NO_PRESS;
    }
}

/* *****************************************************************************
 End of File
 */