#ifndef _DRIVER_UART_H    /* Guard against multiple inclusion */
#define _DRIVER_UART_H

/*
 * 自定义数据类型
 */
// UART2的状态
typedef enum {
    UART2_IDLE = 0,
    UART2_BUFFER_NOT_EMPTY
} UART2_TASK_STATUS;

/*
 * 宏定义
 */
#define SYSCLK              200000000
#define PBCLK2              SYSCLK/2
#define BAUDRATE            115200

/*
 * 接口函数
 */
unsigned int UART2_Init(unsigned int baudrate);
void UART2_Task(void);
void UART2_RX_FIFO_Init(void);
void UART2_RX_FIFO_Ptr_Update(void);

#endif /* _DRIVER_UART_H */

/* *****************************************************************************
 End of File
 */
